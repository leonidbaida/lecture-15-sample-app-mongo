var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var gulpsync = require('gulp-sync')(gulp);
var cssnano = require('gulp-cssnano');
var jade = require('gulp-jade');
var rename = require('gulp-rename');
var wrap = require('gulp-wrap');
var less = require('gulp-less');
var path = require('path');
var watch = require('gulp-watch');

var LIBS = [
    'node_modules/requirejs/require.js',
    'node_modules/jquery/dist/jquery.js',
    'node_modules/jade/runtime.js',
    'node_modules/backbone/backbone.js',
    'node_modules/backbone.marionette/lib/backbone.marionette.js',
    'node_modules/backbone.localstorage/backbone.localstorage.js',
    'node_modules/underscore/underscore.js'
];

gulp.task('build:clear', function () {
    return del(['public']);
});

gulp.task('build:js', function () {
    return gulp.src('src/public-src/**/*.js')
        .pipe(gulp.dest('src/public/'));
});

gulp.task('build:less', function () {
    return gulp.src('src/public-src/**/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest('src/public/'));
});

gulp.task('build:jade', function () {
    return gulp.src('src/public-src/**/*.jade')
        .pipe(jade({client: true}))
        .pipe(wrap('define(["libs/jade"], function(jade) {return <%= contents %>})'))
        .pipe(uglify())
        .pipe(gulp.dest('src/public/'));
});

gulp.task('build:static:node_modules', function () {
    return gulp.src(LIBS)
        .pipe(rename(function (path) {
            if (path.basename == 'runtime') {
                path.basename = 'jade'
            }
        }))
        .pipe(gulp.dest('src/public/js/libs/'));
});

gulp.task('build:static', ['build:static:node_modules']);

gulp.task('build', gulpsync.sync(['build:clear', ['build:js', 'build:less', 'build:jade', 'build:static']]));

gulp.task('default', ['build']);

gulp.task('watch', ['build'], function () {
    watch(['src/**/*', '!./src/public/*', 'data/**/*'], function () {
        gulp.start('build');
    });
});