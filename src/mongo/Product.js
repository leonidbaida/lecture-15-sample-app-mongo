var mongoose = require('./mongoose');

var schema = mongoose.Schema({
    name: String,
    price: Number,
    available: Boolean
}, {
    collection: 'product'
});

var Product = mongoose.model('Product', schema);

module.exports = Product;