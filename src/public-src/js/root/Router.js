define([
    'backbone'
], function (Backbone) {
    return Backbone.Router.extend({

        routes: {
            '': 'root'
        },

        root: function () {
            this.navigate('products', {trigger: true});
        }
    });
});