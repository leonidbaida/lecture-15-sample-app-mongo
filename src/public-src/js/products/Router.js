define([
    'backbone',
    './LayoutView',

    './cart-strip/ItemView',
    '../cart/Service',

    './products/CollectionView',
    './products/Collection'
], function (Backbone, LayoutView, CartStripView, CartService, ProductsView, Products) {
    return Backbone.Router.extend({

        initialize: function (options) {
            this.container = options.container;
        },

        routes: {
            products: 'index'
        },

        index: function () {
            var view = new LayoutView;
            this.container.show(view);

            var cartView = new CartStripView({
                collection: CartService.collection
            });

            var productsCollection = new Products();
            productsCollection.fetch();
            var productsView = new ProductsView({
                collection: productsCollection
            });

            view.cart.show(cartView);
            view.products.show(productsView);
        }
    });
});