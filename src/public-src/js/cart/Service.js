define([
    'backbone',
    'backbone.marionette',
    'localstorage'
], function (Backbone, Marionette, localstorage) {

    var predicate = function (id) {
        return function (model) {
            return model.get('product') == id;
        }
    };

    var Model = Backbone.Model.extend({
        idAttribute: 'product'
    });

    var Collection = Backbone.Collection.extend({
        localStorage: new Backbone.LocalStorage('cart'),
        model: Model
    });

    var Service = Marionette.Object.extend({
        initialize: function () {
            this.collection = new Collection();
            this.collection.fetch();
        },

        _find: function (id) {
            return this.collection.find(predicate(id));
        },

        serialize: function () {
            return this.collection.toJSON();
        },

        add: function (id, quantity) {
            if (quantity === 0) {
                this.remove(id);

            } else {
                var model = this._find(id);
                if (model) {
                    model.set('quantity', quantity || 1);
                } else {
                    model = this.collection.add({
                        product: id,
                        quantity: quantity || 1
                    });
                }
                model.save();
            }
        },

        remove: function (id) {
            var model = this._find(id);
            model.destroy();
        },

        toggle: function (id) {
            if (this.has(id)) {
                this.remove(id);
            } else {
                this.add(id);
            }
        },

        count: function () {
            return this.collection.length;
        },

        has: function (id) {
            return !!this._find(id);
        },

        removeAll: function () {
            while (this.collection.length > 0) {
                this.collection.first().destroy();
            }
        }
    });

    return new Service;
});
